<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use App\Posts;

class testpost implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $product_title;
    protected $excerpt;
    protected $price;

    public function __construct($product_title, $excerpt, $price) {
        $this->product_title = $product_title;
        $this->excerpt = $excerpt;
        $this->price = $price;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $post = new Posts();
        $post->product_title = $this->product_title;
        $post->excerpt = $this->excerpt;
        $post->price = $this->price;
        $post->save();
//        Posts::create($post);
    }

}
