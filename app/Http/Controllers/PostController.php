<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Posts;
use App\Jobs\testpost;

class PostController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $post = Posts::paginate(10);
        return view('post.post', ['post' => $post]);
    }

    public function addpost(request $r) {
        $this->validate($r, [
            'title' => 'required|',
            'excerpt' => 'required|',
            'price' => 'required|'
                ], [
            'title.required' => 'Nhap title'
        ]);
        $product_title = $r->title;
        $excerpt = $r->excerpt;
        $price = $r->price;
        dispatch(new \App\Jobs\testpost($product_title, $excerpt, $price));
        return redirect('/addpost')->with('notification', 'Add Success.');
    }

    public function getedit($id) {
        $p = Posts::find($id);
        return view('post.edit', ['post' => $p]);
    }

    public function postedit(request $r, $id) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $p = Posts::find($id);
        $this->validate($r, [
            'title' => 'required|',
            'excerpt' => 'required|',
            'price' => 'required|'
                ], [
            'title.required' => 'Nhap title'
        ]);
        $p->product_title = $r->title;
        $p->excerpt = $r->excerpt;
        $p->price = $r->price;
        $p->save();
        return redirect('editpost/' . $id)->with('notification', 'Edit Success.');
    }

    public function delete($id) {
        $p = Posts::find($id);
        $p->delete();
        return redirect('/post')->with('notification', 'Delete Success.');
    }

}
