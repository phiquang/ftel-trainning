@include('post.layouts.header')
@include('post.layouts.sidebar')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Forms Edit</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Post
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            @if(count($errors) > 0)
                            <div class="alert alert-warning">
                                @foreach($errors->all() as $err)
                                {{$err}}<br>
                                @endforeach
                            </div>
                            @endif
                            @if(session('notification'))
                            <div class="alert alert-success">
                                {{session('notification')}} <a href="/post"> List</a>
                            </div>
                            @endif
                            <form method="POST" action="/editpost/{{$post->id}}">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group input-group">
                                    <label>Title</label>
                                    <input class="form-control" name="title" value="{{$post->product_title}}">
                                </div>
                                <div class="form-group input-group">
                                    <label>Excerpt</label>
                                    <textarea class="form-control" name="excerpt" rows="3">{{$post->excerpt}}</textarea>
                                </div>
                                <div class="form-group input-group">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price" value="{{$post->price}}">
                                </div>
                                <button type="submit" class="btn btn-default">Submit</button>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="/posttemplate/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/posttemplate/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/posttemplate/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/posttemplate/dist/js/sb-admin-2.js"></script>