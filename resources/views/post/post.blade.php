<!--<h1>Hi</h1>-->
<?php
//var_dump($post); 
//foreach($post as $p){
//    echo $p['product_title'];
//}
?>
<!--@foreach ($post as $p)
{{ $p->product_title }}<br>
@endforeach
{!! $post->links() !!}-->
<!--{{ $post->render() }}-->
<!DOCTYPE html>

@include('post.layouts.header')
@include('post.layouts.sidebar')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tables</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <button class="btn btn-default"><i class="fa fa-plus-square"></i><a href="/addpost">Add</a></button>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    @if(session('notification'))
                    <div class="alert alert-success">
                        {{session('notification')}}
                    </div>
                    @endif
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Excerpt</th>
                                <th>Price</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($post as $p)
                            <tr>
                                <td>{{ $p->product_title }}</td>
                                <td>{{ $p->excerpt }}</td>
                                <td>{{ $p->price }}</td>
                                <td>
                                    <button><a title="Edit" href="/editpost/{{$p->id}}"><i class="fa fa-edit"></i></a></button>
                                    <button><a title="Delete" href="/delete/{{$p->id}}"><i class="fa fa-trash"> </i> </a></button>
                                </td>
                            </tr>
                            @endforeach
                            {!! $post->links() !!}
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="posttemplate/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="posttemplate/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="posttemplate/vendor/metisMenu/metisMenu.min.js"></script>
<!-- DataTables JavaScript -->
<script src="posttemplate/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="posttemplate/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="posttemplate/vendor/datatables-responsive/dataTables.responsive.js"></script>
<!-- Custom Theme JavaScript -->
<script src="posttemplate/dist/js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function () {
    $('#dataTables-example').DataTable({
        responsive: true
    });
});
</script>

</body>

</html>
