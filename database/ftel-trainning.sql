-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2019 at 03:33 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ftel-trainning`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_23_034048_create_article_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(50) NOT NULL,
  `product_title` varchar(200) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `excerpt` mediumtext COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `price` varchar(50) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `created_at` varchar(100) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `last_update` varchar(100) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `product_title`, `excerpt`, `price`, `created_at`, `last_update`) VALUES
(2, 'Áo len', 'ok ok', '500000', '21/01/2018 - 14:33:25', '2019-01-21 02:50:31'),
(3, 'Quần jean lưng cao', 'Quần jean lưng cao mới đẹp duyên dáng xinh xắn đáng yêu xuân hè năm 2018 trị giá 350.000 VNĐ nay chỉ còn 250.000 VNĐ', '400000', '21/01/2018 - 14:34:09', NULL),
(4, 'Quần Skinny rách gối', 'Quần Skinny rách gối mới đẹp xuân hè năm 2018 trị giá 230.000 VNĐ nay chỉ còn 149.000 VNĐ', '100000', '21/01/2018 - 14:34:48', NULL),
(5, 'Quần baggy', 'Quần baggy mới đẹp duyên dáng xinh xắn đáng yêu xuân hè năm 2018 trị giá 270.000 VNĐ nay chỉ còn 169.000 V', '300000', '22/01/2018 - 00:23:47', NULL),
(6, 'Quần culottes nỉ dài', 'Quần culottes nỉ dài trị giá 350.000 VNĐ nay chỉ còn 129.000 VNĐ luôn mang đến vẻ sang trọng cũng như thời trang hơn cho các nàng.', '150000', '27/01/2018 - 09:43:22', NULL),
(7, 'Quần legging As', '''Quần legging As mới đẹp duyên dáng xinh xắn đáng yêu xuân hè năm 2018 trị giá 230.000 VNĐ nay chỉ còn 139.000 VNĐ', '230000', '27/01/2018 - 09:49:58', NULL),
(8, 'QUẦN PHỐI CÚP THÂN TRƯỚC', 'QUẦN PHỐI CÚP THÂN TRƯỚC', '769000', '21/04/2018 - 00:16:15', NULL),
(9, 'Áo thun Milan', 'Áo thun Milan mới đẹp duyên dáng xinh xắn đáng yêu hè thu năm 2017 trị giá 230.000 VNĐ nay chỉ còn 99.000 VNĐ', '200000', '04/05/2018 - 11:16:33', NULL),
(10, 'Áo thun mickey', 'Áo thun mickey được thiết kế với kiểu dáng cổ tròn tay ngắn mang lại cảm giác thoải mái trẻ trung năng động hơn trong những ngày hè trị giá 250.000 VNĐ nay chỉ còn 129.000 VNĐ', '250000', '04/05/2018 - 11:20:04', NULL),
(11, 'Áo croptop phối nút', 'Áo croptop phối nút mới đẹp sexy cá tính trị giá 250.000 VNĐ nay chỉ còn 129.000 VNĐ', '200000', '04/05/2018 - 11:24:04', NULL),
(12, 'Áo vest peplum', 'Áo vest peplum thời trang công sở trẻ trung, thanh lịch khi phối cùng nhiều trang phục khác trị giá 300.000 VNĐ nay chỉ còn 220.000 VNĐ.', '500000', '04/05/2018 - 11:30:34', NULL),
(13, 'Áo măng tô nữ kaki form dài cột nơ', 'Áo khoác măng tô nữ kaki form dài cột nơ trị giá 650.000 VNĐ nay chỉ còn 450.000 VNĐ dễ dàng phối cùng nhiều trang phục khác nhau, mang đế phong cách mới cho mùa thu đông 2016.', '800000', '04/05/2018 - 11:33:50', NULL),
(14, 'Áo khoác măng tô viền tay', 'Áo khoác măng tô viền tay mới đẹp duyên dáng xinh xắn đáng yêu hè thu năm 2017 trị giá 750.000 VNĐ nay chỉ còn 399.000 VNĐ', '700000', '04/05/2018 - 11:40:29', NULL),
(15, 'Chân váy phối da', 'Chân váy phối da 0506 trị giá 300.000 VNĐ nay chỉ còn 129.000 VNĐ mang đến cho các bạn gái sự trẻ trung và thời trang hơn.', '300000', '04/05/2018 - 11:53:09', NULL),
(16, 'Đầm toppy', 'Đầm toppy mới đẹp duyên dáng xinh xắn đáng yêu hè năm 2018 trị giá 320.000 VNĐ nay chỉ còn 210.000 VNĐ', '200000', '04/05/2018 - 12:01:47', NULL),
(17, 'Đầm body sọc cổ tim', 'Đầm body sọc cổ tim mới đẹp duyên dáng xinh xắn đáng yêu hè năm 2018 trị giá 280.000 VNĐ nay chỉ còn 160.000 VNĐ', '280000', '04/05/2018 - 12:16:18', NULL),
(18, 'Đầm ren 2 dây cắt lazer ', 'Đầm ren 2 dây cắt lazer 0605 trẻ trung và gợi cảm hơn với thiết kế cắt lazer ren hoa đẹp mắt trị giá 450.000 VNĐ nay chỉ còn 240.000 VNĐ.', '450000', '04/05/2018 - 12:21:23', NULL),
(19, 'Set bộ vest kèm áo khoác sọc', 'Set bộ vest kèm áo khoác sọc trị giá 500.000 VNĐ nay chỉ còn 350.000 VNĐ mang đến cho các bạn gái sự trẻ trung và thời trang hơn.', '500000', '04/05/2018 - 12:26:18', NULL),
(20, 'Set áo tay dài váy yếm thu đông', 'Set áo tay dài váy yếm thu đông mới đẹp duyên dáng xinh xắn đáng yêu thu đông năm 2017 trị giá 300.000 VNĐ nay chỉ còn 199.000 VNĐ', '3200000', '04/05/2018 - 12:29:23', '2019-01-15 03:12:31'),
(21, 'Jumpsuit ngắn phối màu', 'Jumpsuit ngắn phối màu mới đẹp duyên dáng xinh xắn đáng yêu hè năm 2018 trị giá 360.000 VNĐ nay chỉ còn 230.000 VN', '360000', '04/05/2018 - 12:32:00', NULL),
(22, 'Áo khoác dạ 2 túi phối lông', 'Áo khoác dạ 2 túi phối lông mới đẹp, xinh xắn, dễ thương, năng động, thích hợp cho những dịp dạo phố hay những ngày trời se lạnh: 600.000 VNĐ nay chỉ còn 499.000 VNĐ', '499000', '14/11/2018 - 04:34:22', NULL),
(23, 'Áo sơ mi sọc vòng cổ', '', '150000', '14/11/2018 - 04:55:33', NULL),
(24, 'Đầm thun polo cổ trụ dây kéo', 'Giá gốc: 300.000 đ', '150000', '14/11/2018 - 05:18:26', NULL),
(25, 'Đầm ren 2 dây đính hạt', 'Đầm ren 2 dây đính hạt, xinh xắn, dễ thương, quyến rũ, hè năm 2018 trị giá: 299.000 VNĐ nay chỉ còn 199.000 VNĐ', '199000', '14/11/2018 - 05:20:33', NULL),
(26, 'Chân váy dài phối nút cột eo', 'Chân váy dài phối nút cột eo mới đẹp, dễ thương, dễ dàng kết hợp với nhiều kiểu áo khác nhau trị giá: 260.000 VNĐ nay chỉ còn 160.000 VNĐ', '160000', '14/11/2018 - 05:25:34', NULL),
(27, 'Chân váy dài xếp ly', 'Chân váy dài xếp ly mới đẹp, dễ thương, dễ dàng kết hợp với nhiều kiểu áo khác nhau trị giá: 270.000 VNĐ nay chỉ còn 170.000 VNĐ', '170000', '14/11/2018 - 05:28:39', NULL),
(28, 'Set áo cổ nơ chân váy ôm tua rua', 'Set áo cổ nơ chân váy ôm tua rua dễ thương, xinh xắn và gợi cảm là những gì mà chiếc đầm mang đến cho các nàng trị giá: 320.000 VNĐ nay chỉ còn 220.000 VNĐ', '220000', '14/11/2018 - 05:31:33', NULL),
(29, 'Set áo peplum chân váy ôm thắt eo', 'Set áo peplum chân váy ôm thắt eo xinh đẹp và dễ thương cho những buổi dạo phố cùng bạn bè trị giá: 300.000 VNĐ nay chỉ còn 199.000 VNĐ', '199000', '14/11/2018 - 05:33:22', NULL),
(30, 'Phí Thành Quang', 'aa', '1', '14/01/2019 - 04:13:27', NULL),
(31, 'abcd', 'aabb', '1', '14/01/2019 - 04:15:09', NULL),
(34, 'qwe', 'qq', '2', '14/01/2019 - 11:19:29', NULL),
(35, 'asd', 'sad', '2', '14/01/2019 - 11:49:49', NULL),
(39, 'rewer', 'sad', '23456', '2019-01-15 - 09:31:57', NULL),
(40, 'fgh', 'dfg', '6666', '2019-01-15 - 09:54:48', '2019-01-15 02:57:14'),
(41, 'tttt', 'ttt', '888', '2019-01-15 - 10:26:45', '2019-01-15 03:28:27'),
(42, 'quang', 'quang', '234', '2019-01-21 - 09:55:36', NULL),
(43, 'thanhquang', 'tq', '10', '2019-01-21 - 09:59:25', NULL),
(44, 'tttt', 'qw', '234', '2019-01-21 - 10:02:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Phí Thành Quang', 'phiquang.97@gmail.com', '$2y$10$ZIgXC4umVzqBKdAEyNoRLePMCMShXBPCKo88wVtkUONY6GWqF4xwO', 'G9o1MhEkYLseHd9JJpybofVBoYHrDGnLfA8GGJKNlpSH3QxZNRvvjOhl2sXK', '2019-01-22 20:49:52', '2019-01-22 20:49:52'),
(2, 'Quang', 'quang@gmail.com', '$2y$10$uaxcCOGQwCU407XQuErYWOA926RWYiL4jNylZ2zL2sY2D55XplNCW', NULL, NULL, NULL),
(3, 'Enos Bahringer', 'langosh.jarrod@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'xi6VUeshy9', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(4, 'Ahmad Hayes', 'isai.spencer@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'E84YUC7y4d', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(5, 'Trudie Grant', 'johnston.santina@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', '3ZpJHGIujH', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(6, 'Coy Wilderman', 'brandyn95@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'Oh4OJWhCXg', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(7, 'Mateo Reinger IV', 'rebekah29@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'Kxf0mHoib2', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(8, 'Dr. Moses Cartwright', 'taryn76@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'C0tkKFJ6eS', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(9, 'Prof. Lavern Kreiger PhD', 'jovani19@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'wDUPicXRsd', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(10, 'Magnus Wuckert DDS', 'bins.hailie@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'tKVq9YRr7w', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(11, 'Frank O''Keefe II', 'maggie.flatley@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'hk5QWkkfcQ', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(12, 'Chase Beier', 'walker.paxton@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'JLLctniBy9', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(13, 'Viola Kiehn', 'joel90@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'd0NsaFY0VK', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(14, 'Veronica Heller MD', 'labadie.skylar@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'bWg06Xnjat', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(15, 'Grayson McKenzie', 'watsica.keshawn@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'Fz0WUTNgvW', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(16, 'Trevor O''Reilly', 'ykemmer@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'VFVbA3TTef', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(17, 'Kory Denesik', 'elza.carter@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'BryuxMGytm', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(18, 'Mr. Ottis Schuppe PhD', 'nkoelpin@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'Ozaj3DOjX8', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(19, 'Brock Ortiz Sr.', 'rhudson@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', '3wEEsGLhFx', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(20, 'Mr. Gunnar Langworth', 'mariah.friesen@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'nZJ9xm3EaB', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(21, 'Prof. Emiliano Powlowski', 'maximilian12@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', '5IMXOySu8N', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(22, 'Stevie Gusikowski', 'donnelly.sofia@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'eWF1qE4PCl', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(23, 'Ronaldo Wyman', 'aron.terry@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'jHZ8TJ5dk1', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(24, 'Courtney Gutkowski', 'wisoky.anahi@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'co3MQwXRd7', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(25, 'Raleigh Ruecker MD', 'kylee.ratke@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', '1OG1SJG1RH', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(26, 'Ms. Myrtis Weber PhD', 'agibson@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'grTsUvMSKA', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(27, 'Mrs. Magali Steuber II', 'julien.kohler@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'rfMPxxZ7VE', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(28, 'Dr. Yasmin Johns PhD', 'ally81@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'y6rz9V7NNE', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(29, 'Dr. Chris Bogisich', 'isabell.trantow@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'w82v5rwqRB', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(30, 'Nellie Gislason', 'jalen87@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', '8sppyMiu66', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(31, 'Ronny Kling', 'ritchie.jessie@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'zzH27lsN26', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(32, 'Zoila Gutkowski', 'marcos.witting@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'wl2CP6pSTm', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(33, 'Hal Heller', 'cielo.dubuque@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'fRm9AsVIWH', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(34, 'Clemmie Dach', 'kenny51@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'v7HIEGzoIz', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(35, 'Royal Schulist', 'nicholaus65@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'cBMdMgTgDt', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(36, 'Prof. Friedrich Fisher Sr.', 'smith.tillman@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'sgeC47NIE9', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(37, 'Mr. Emery Harvey III', 'xschoen@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', '8LMTjyjt9E', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(38, 'Ms. Josiane Miller Jr.', 'minerva.oberbrunner@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'tvn2J0H6LK', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(39, 'Meaghan Franecki', 'nmayert@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'oqHMOrL0j8', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(40, 'Marcus Wiegand', 'may71@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'ZWgEClkyP8', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(41, 'Dion Crona', 'uharris@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'tSr0GqG7hH', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(42, 'Kianna Hintz I', 'margarett12@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'HhCcJqrymL', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(43, 'Jameson Hill', 'jpaucek@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'covaJRg56r', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(44, 'Mr. Damon Stark', 'krystel97@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', '89hB15t1vp', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(45, 'Marc Becker', 'rkub@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'ZYQomk4gdq', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(46, 'Earnest Schmidt DVM', 'lward@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'z2McL0vNQQ', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(47, 'Enrique Langosh DVM', 'eileen54@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', '86gRgW1OYt', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(48, 'Yvette Hill III', 'dubuque.renee@example.net', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'DJze1bAXtF', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(49, 'Prof. Troy Kautzer I', 'duane.douglas@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'zrd4hHXicV', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(50, 'Angelo Schiller', 'schmeler.greg@example.org', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'nt8B3jhhpr', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(51, 'Allie Boehm', 'hschinner@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'dBz5y3SLiE', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(52, 'Nigel Bashirian', 'ludwig06@example.com', '$2y$10$0OpxMGGtbtZggWBsSllYqu7LRk/dABUuShI0Vox2V34SOZDVRiKlG', 'gU19FLDSTO', '2019-01-22 21:07:41', '2019-01-22 21:07:41'),
(53, 'Sydni Zieme', 'friesen.rosamond@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'OVFiG2OXGs', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(54, 'Clifford Hyatt', 'kkemmer@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'jJ2PxEFdLv', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(55, 'Lloyd Howell DDS', 'rafaela.walker@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'UpV9mV2odq', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(56, 'Antonietta Gibson', 'keaton53@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'Sd4JAND7La', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(57, 'Fabian Pagac', 'mcclure.dillon@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'jQ69nX1SXF', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(58, 'Elmira McDermott', 'ova.gerhold@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'AfhJq01vUH', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(59, 'Willis Strosin', 'beulah02@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', '55Z5dV2kja', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(60, 'Jordyn Abshire', 'lhane@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'pktfwhQc8s', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(61, 'Dr. Kianna Gislason', 'spfeffer@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'v6VCBqNLah', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(62, 'Ms. Rosalyn Wehner', 'amy.paucek@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'M8hAjGNVPo', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(63, 'Prof. Neal Stehr', 'schuppe.rick@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', '8W8NImmM75', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(64, 'Miss Tanya Schoen', 'heller.axel@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', '24ou9n5hXp', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(65, 'Lemuel Spinka I', 'elenor.osinski@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'xoJ53DV8P4', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(66, 'Mr. Tyreek Miller', 'treutel.telly@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', '3WQEJ2pkgi', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(67, 'Isadore Romaguera II', 'bkuhn@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'U9x3wwzsBu', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(68, 'Quinten Stehr', 'aritchie@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'b6PGIDrrrO', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(69, 'Alfreda Hammes', 'ruby.dickens@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'RHS1h201yy', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(70, 'Edyth Kilback', 'eweissnat@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'fOLvCDT2dm', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(71, 'Sylvan Prosacco I', 'halvorson.lela@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'uAFW6WVr3G', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(72, 'Hailee Armstrong IV', 'hammes.gillian@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'ndoyLWkLnz', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(73, 'Haylie Jacobs', 'bartoletti.estrella@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'i0HQTDYDsy', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(74, 'Dr. Jeramy Funk III', 'helga.lowe@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'z53WVoqlD0', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(75, 'Soledad Kuphal', 'qabbott@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', '14K6SjGTS4', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(76, 'Jackeline Leannon III', 'johnnie89@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'eFMM8QaSga', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(77, 'Mr. Doris Rice', 'kavon47@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'Tu2StpUnqy', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(78, 'Miss Alexandra Nader III', 'zkeebler@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'PHEOSMK8Pt', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(79, 'Prof. Gloria Schuster MD', 'sidney21@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'hEAZcm02gc', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(80, 'Willow Christiansen MD', 'susan.hoeger@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'YenKWoNGqS', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(81, 'Freddy Waelchi', 'uriel20@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'wlUyQ0bREu', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(82, 'Elenora Will', 'brandon41@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'brJmFAi3AE', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(83, 'Prof. Winston Witting II', 'deven.kuhlman@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'CDTv4d89mO', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(84, 'Prof. Savannah Stroman', 'dolly.becker@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'sM7zwYwwqW', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(85, 'Rafaela Flatley', 'mmclaughlin@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'nmh4eWLQdg', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(86, 'Jaquelin Dickinson', 'schinner.tremayne@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'szieH7nW2s', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(87, 'Laurel Erdman', 'deshawn32@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'rFmrEY28a6', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(88, 'Dr. Ian Larkin PhD', 'emilio90@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'PPWKoCw3YA', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(89, 'Elenor Dooley', 'kferry@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'HvrdjGDhmf', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(90, 'Kariane Carter', 'ehermann@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'NS5rJxPxOz', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(91, 'Mr. Ron Macejkovic DVM', 'brenna60@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'b7t5JhWpUf', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(92, 'Zackery Carroll', 'mayert.winifred@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'greK7qZXwW', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(93, 'Dr. Kamryn McKenzie PhD', 'jasper42@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'YQakwFHPLB', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(94, 'Dr. Hoyt Ullrich DVM', 'leonor97@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'dSt1w0dxFs', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(95, 'Prof. Gerald Schulist', 'orval.predovic@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'MuyoFUmV9B', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(96, 'Dr. Alexandra Bruen', 'helena.bergstrom@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'rak0FPRqnv', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(97, 'Dr. Shirley Gorczany', 'mann.mazie@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'xRvd9KBVUU', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(98, 'Dr. Mac Hermiston III', 'swaniawski.jadyn@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'NlAmgC1Kms', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(99, 'Mr. Halle Wintheiser Sr.', 'alize.schaefer@example.net', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'vuqUqhIpTB', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(100, 'Hayden Robel I', 'robel.josefa@example.com', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', '5PSDDktmZP', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(101, 'Estrella Mohr Jr.', 'gleichner.shanie@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 'oP4oaWfkDs', '2019-01-22 21:08:20', '2019-01-22 21:08:20'),
(102, 'Miss Nannie Heller', 'ledner.astrid@example.org', '$2y$10$MMIOAnjxeqCh.zqw16KA4OZuZLkPwCcLR/VT1wY2AksJ3wmvBqem2', 't3k2j9b5Ym', '2019-01-22 21:08:20', '2019-01-22 21:08:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
