<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('post', 'PostController@index');
Route::get('addpost', function() {
    return view('post.add');
});
Route::post('addpost', 'PostController@addpost');
Route::get('editpost/{id}', 'PostController@getedit');
Route::post('editpost/{id}', 'PostController@postedit');
Route::get('delete/{id}', 'PostController@delete');

